export class Meal {
    title: string;
    imageUrl: string;
    kkal: number;
    description: string;
    recipe: string;
}