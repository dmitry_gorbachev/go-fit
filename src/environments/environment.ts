// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBgOdcQXlUBU23oNpSh_MEZcVczxqqv35M",
    authDomain: "reginarus-go.firebaseapp.com",
    databaseURL: "https://reginarus-go-default-rtdb.firebaseio.com/",
    projectId: "reginarus-go",
    storageBucket: "reginarus-go.appspot.com",
    messagingSenderId: "987135104799",
    appId: "1:987135104799:web:416dac2dd4c91986b7c22a",
    measurementId: "G-0D4MB8WTPL"  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
